-- Task 4 :DOCTOR
-- Doctor#
-- DoctorName
-- Secretary
-- Patient#
-- PatientName
-- PatientDOB
-- PatientAddress
-- Prescription#
-- Drug
-- Date
-- Dosage

CREATE DATABASE IF NOT EXISTS `db_design_drills`;

USE `db_design_drills`;

CREATE TABLE `PRESCRIPTION`(
	`Prescription_id` INT PRIMARY KEY,
    `Drug` VARCHAR(255) NOT NULL,
    `Date` DATE NOT NULL,
    `Dosage` CHAR(25) NOT NULL
);

CREATE TABLE `PATIENT`(
	`Patient_id` INT PRIMARY KEY,
    `Patient_name` VARCHAR(255) NOT NULL,
    `Date_of_Birth` DATE NOT NULL,
    `Address` TEXT NOT NULL,
    `Prescription_id` INT,
    `Doctor_id` INT,
    FOREIGN KEY (Prescription_id) REFERENCES PRESCRIPTION(Prescription_id)
);

CREATE TABLE `SECRETARY`(
	`Secretary_id` INT PRIMARY KEY,
    `Secretary_name` VARCHAR(255) NOT NULL,
    `Patient_id` INT,
    FOREIGN KEY (Patient_id) REFERENCES PATIENT(Patient_id)
);

CREATE TABLE `DOCTOR`(
	`Doctor_id` INT PRIMARY KEY,
    `Doctor_name` VARCHAR(255) NOT NULL,
    `Secretary_id` INT,
    FOREIGN KEY (Secretary_id) REFERENCES SECRETARY(Secretary_id)
);

