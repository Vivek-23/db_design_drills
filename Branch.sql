-- Branch
-- Branch_Addr
-- ISBN
-- Title
-- Author
-- Publisher
-- Num_copies

CREATE DATABASE IF NOT EXISTS `db_design_drills`;

USE `db_design_drills`;

CREATE TABLE `BRANCH`(
	`Branch_id` INT PRIMARY KEY,
    `Branch_name` VARCHAR(255) NOT NULL,
    `Branch_Address` TEXT NOT NULL
);

CREATE TABLE `PUBLISHER`(
	`Publisher_id` INT PRIMARY KEY,
    `Publisher_name` VARCHAR(255) NOT NULL,
    `Num_Copies` INT NOT NULL
);

CREATE TABLE `BOOKS` (
	`ISBN` INT PRIMARY KEY,
    `Title` VARCHAR(255) NOT NULL,
	`Author` VARCHAR(255) NOT NULL,
    `Publisher_id` INT,
    `Branch_id` INT,
    FOREIGN KEY (Publisher_id) REFERENCES PUBLISHER(Publisher_id),
    FOREIGN KEY (Branch_id) REFERENCES BRANCH(Branch_id)
);
