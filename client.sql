-- Client#
-- Name
-- Location
-- Manager#
-- Manager_name
-- Manager_location
-- Contract#
-- Estimated_cost
-- Completion_date
-- Staff#
-- Staff_name
-- Staff_location

CREATE DATABASE IF NOT EXISTS `db_design_drills`;

USE `db_design_drills`;

CREATE TABLE `MANAGER`(
	`Manager_id` INT PRIMARY KEY,
    `Manager_name` VARCHAR(255) NOT NULL,
    `Manager_location` TEXT NOT NULL
);

CREATE TABLE `CONTRACT`(
	`Contract_id` INT PRIMARY KEY,
    `Estimated_cost` FLOAT NOT NULL,
    `Completion_Date` DATE NOT NULL
);

CREATE TABLE `STAFF`(
	`Staff_id` INT PRIMARY KEY,
    `Staff_name` VARCHAR(255) NOT NULL,
    `Staff_location` TEXT NOT NULL
);

CREATE TABLE `CLIENT`(
	`Client_id` INT PRIMARY KEY,
    `Name` VARCHAR(255) NOT NULL,
    `Location` TEXT NOT NULL,
    `Manager_id` INT,
    `Contract_id` INT,
    `Staff_id` INT,
    FOREIGN KEY (Manager_id) REFERENCES MANAGER(Manager_id),
    FOREIGN KEY (Contract_id) REFERENCES CONTRACT(Contract_id), 
    FOREIGN KEY (Staff_id) REFERENCES STAFF(Staff_id)
);



