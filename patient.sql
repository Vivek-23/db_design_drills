-- Task 3: PATIENT
-- Patient#
-- Name
-- DOB
-- Address
-- Prescription#
-- Drug
-- Date
-- Dosage
-- Doctor
-- Secretary

CREATE DATABASE IF NOT EXISTS `db_design_drills`;

USE `db_design_drills`;

CREATE TABLE `DOCTOR`(
	`Doctor_id` INT PRIMARY KEY,
    `Doctor_name` VARCHAR(255) NOT NULL
);

CREATE TABLE `SECRETARY`(
	`Secretary_id` INT PRIMARY KEY,
    `Secretary_name` VARCHAR(255) NOT NULL,
    `Doctor_id` INT,
    FOREIGN KEY (Secretary_id) REFERENCES DOCTOR(Doctor_id)
);

CREATE TABLE `PRESCRIPTION`(
	`Precription_id` INT PRIMARY KEY,
    `Drug` VARCHAR(255) NOT NULL,
    `Date` DATE NOT NULL,
    `Dosage` CHAR(10) NOT NULL
);

CREATE TABLE `PATIENT`(
	`Patient_id` INT PRIMARY KEY,
    `Patient_name` VARCHAR(255) NOT NULL,
    `Patient_DOB` DATE NOT NULL,
    `Patient_Address` TEXT NOT NULL,
    `Prescription_id` INT,
    `Secretary_id` INT,
    FOREIGN KEY (Prescription_id) REFERENCES PRESCRIPTION(Precription_id),
    FOREIGN KEY (Secretary_id) REFERENCES SECRETARY(Secretary_id)
);



